set terminal pdf color solid linewidth 5
set output "single.pdf"

set xlabel "BlockDimx.y"
set title "performance of cudaSingle() and cudaSingleX2()"
set yrange [0:6]
set xrange [0:36]
set xtics 4 
set key bottom

p 'single.txt' u 1:2 w lp title 'cudaSingle', 'single.txt' u 1:3 w lp title 'cudaSingleX2'
