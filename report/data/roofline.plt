set terminal pdf color dashed  linewidth 4
set output "roofline.pdf"

set xlabel "computation intensity (Flops/Byte)"
#set title "performance of cudaSingle() and cudaSingleX2()"
set yrange [0:12]
set xrange [0:1.5]
set xtics 0.5
set key left

turn=0.33375
ym=10.68
x2=1.083
f1(x)= (x<0.33375) ? 32*x : 10.68

set arrow from 0.33375,0 to 0.33375, f1(0.33375)  nohead ls 3
set xtics add (turn, turn)
set ytics add(f1(turn) f1(turn))

set arrow from x2,0 to x2, ym nohead ls 2
set xtics add (x2,x2)

plot f1(x) title 'roofline model'


