set terminal pdf color solid
set output "gpu.pdf"

set xlabel "different implementation"
set title "various GPU-optimized performance "
set yrange [0:4]
set key outside

# Make the x axis labels easier to read.
#set xtics rotate out, size to 8
set xtics rotate by -45 scale 0  font  ",8"
# Select histogram data
set style data histogram
set style histogram clustered gap 1
# Give the bars a plain fill pattern, and draw a solid line around them.
set style fill solid border
set boxwidth 0.9

plot for [COL=2:5] 'gpu.txt' using COL:xticlabels(1) title columnheader
