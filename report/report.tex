%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\PassOptionsToPackage{table}{xcolor}
\documentclass[fleqn,10pt]{SelfArx} % Document font size and equations flushed left

\setlength{\columnsep}{0.55cm} % Distance between the two columns of text
\setlength{\fboxrule}{0.75pt} % Width of the border around the abstract

\definecolor{color1}{RGB}{0,0,90} % Color of the article title and sections
\definecolor{color2}{RGB}{0,20,20} % Color of the boxes behind the abstract and headings

\newlength{\tocsep} 
\setlength\tocsep{1.5pc} % Sets the indentation of the sections in the table of contents
\setcounter{tocdepth}{3} % Show only three levels in the table of contents section: sections, subsections and subsubsections

\usepackage{lipsum} % Required to insert dummy text
\usepackage{amsmath} 
\usepackage[lined, boxed, linesnumbered, commentsnumbered]{algorithm2e}
\usepackage[ampersand]{easylist}
\usepackage{float}
\usepackage{xcolor}
\usepackage{tabularx}
%----------------------------------------------------------------------------------------
%	ARTICLE INFORMATION
%----------------------------------------------------------------------------------------

%\JournalInfo{Journal, Vol. XXI, No. 1, 1-5, 2013} % Journal information
%\Archive{Additional note} % Additional notes (e.g. copyright, DOI, review/research article)
%\JournalInfo{. } % Journal information
%\Archive{ .} % Additional notes (e.g. copyright, DOI, review/research article)

\PaperTitle{CPU and GPU optimization in finding initial condition for Kuramoto Sivashinsky equation} % Article title

\Authors{Xiong Ding\textsuperscript{1}*,\ GTID: 902934749} % Authors
\affiliation{\textsuperscript{1}\textit{School of Physics, Georgia Institute of Technology, Atlanta, GA, 30308}} % Author affiliation
\affiliation{*\textbf{Corresponding author}: xding@gatech.edu} % Corresponding author

\Keywords{Kuramoto-Sivashinsky,\  Multi-threading,\ GPU } % Keywords - if you don't want any simply remove all the text between the curly brackets
\newcommand{\keywordname}{Keywords} % Defines the keywords heading name

%--------------------------------------------------------------------------------------
%	USER DEFINITION
%--------------------------------------------------------------------------------------
\newcommand{\KSe}{Kuramoto Sivashinsky equation}

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\Abstract{
	High performance computing plays an important role in non-linear dynamic research.
	Finding periodic orbits in chaotic systems requires large number of good initial
	conditions, which is computationally expensive sometimes. In this report, I 
	implement CPU and GPU optimizaions to find relative good initail conditions for
	\KSe\ \cite{journals/siamads/CvitanovicDS10}.
	Our result shows that the \textit{icc+Cilk} implementation has the best
	performance of all the multi CPU implementations, and the GPU implementation has
	better performance if register usage is considered.
	}

%----------------------------------------------------------------------------------------

\begin{document}

\flushbottom % Makes all text pages the same height

\maketitle % Print the title and abstract box

\tableofcontents % Print the contents section

\thispagestyle{empty} % Removes page numbering from the first page

%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------

\section{Introduction} % The \section*{} command stops section numbering

%\addcontentsline{toc}{section}{\hspace*{-\tocsep}Introduction} % Adds this section to the table of contents with negative horizontal space equal to the indent for the numbered sections

	\KSe\ \eqref{eq:kse} has been investigated intensively for it chaotic behavior, and the periodic
	orbits on the attractor plays an important role in characterizing the phase space.
	Good initial condition is required to initialize the 
	multi-shooting method, which enables us to locate periodic orbits; However, it is time consuming to obtain relatively large
	number of good initial conditions, especially when the period is larger than 200.

	\begin{equation}
	 u_t=-\frac{1}{2}(u^2)_x-u_{xx}-u_{xxxx}
	 \label{eq:kse}
	\end{equation}

	In this project, I try to optimize the code which generates good initial conditions based on
	CPU and GPU parallel computing.
	

\subsection{Description of the problem and the naive pseudo code}
	We use the \KSe\ generator to produce $Nb$ long vector sequences, each of which consists of $L$
	vectors. Each vector has the same length $N2=30$. Our task is to calculate the distance between
	latter vectors and the first vector in each sequence; at the same time, we also need to calculate
	the distance after reflection $\hat{x}'=-x$ when symmetry of the problem is taken into account.
	And then each vector is classified into four categories according to its distance to the 
	first vector.
	Therefore the naive pseudo code looks like Algorithm \ref{naivecode}.

	%\begin{figure*}[ht]\centering
	\begin{algorithm}
	\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}

	\Input{$Nb$ sequences of vectors, each of which has $L$ vectors $v_{j}$}
	\Output{distance between latter vectors and the first vector in each sequence; the value of each vector.}
	\BlankLine
	\emph{kssovle() generates \KSe\ vector sequences}\;
	initialize\;
	\For{$i\leftarrow 1$ \KwTo $Nb$}{
		\For{$j\leftarrow 1$ \KwTo $L$}{
			$s1=s2=0$\;
			\For{$k\leftarrow 1$ \KwTo $30$}{
				$s1+=(v_{j}(k)-v_{1}(k))^{2}$\;
				$s2+=(v_{j}(k)+v_{1}(k))^{2}$\;
			}
			$dif1(j)=sqrt(s1)$; \tcp*[r]{ \small{\textsf{the distance between $i_{th}$ vector and the first vector}} }\
			$dif2(j)=sqrt(s2)$; \tcp*[r]{ \small{\textsf{the reflected distance between $i_{th}$ vector and the first vector}} }\
			\uIf(\tcp*[r]{ \small{\textsf{classify the four cases}}}){both $dif1(j)$ and $dif2(j)$ $> tol$}{
				Set $label[i][j]=0$\;
			}
			\uElseIf{$dif1(j)<tol$ and $dif2(j) > tol$}{
				Set $label[i][j]=1$\;
			}
			\uElseIf{$dif1(j)>tol$ and $dif2(j) < tol$}{
				Set $label[i][j]=2$\;
			}
			\Else{
				Set $label[i][j]=3$\;
			}
		}

	}
	\caption{Naive pseudo code}\label{naivecode}
	\end{algorithm}
	
	%\end{figure*}

	\paragraph{Complexity and parallelism of the problem}
		There are three levels of loops: the outer loop goes from $1 \rightarrow Nb$; the middle loop
		goes from $1 \rightarrow L$, and the inner loop has $30$ multiplications of single precision numbers.
		$Nb$ is kept constant,
		and the only variable is $L$, so the complexity of the algorithm is just $O(L)$.

		The total \textbf{work} is $W=\alpha \cdot Nb \cdot L \cdot 30$, where $\alpha$ is constant number,
		and the \textbf{span} is just $3\alpha$, so the \textbf{parallism} is $P=min(10\cdot Nb \cdot L,\ nT)$,
		where $nT$ is the number of computing threads.
		
\subsection{Testing Platform}
\paragraph{Hardware}
	The jinx cluster in CSE department of Georgia Tech is used as the platform to test various versions of Algorithm \ref{naivecode}.
	There are three distinct hardware configurations of the thirty nodes in jinx cluster, but one node just meets my need:
	HP sl390s, whose configuration is listed below.

	\begin{easylist}[itemize]
	\ListProperties(Style2*=---)

		& 2 Intel Xeon X5650 6-core processors
				&& clock speed : 2.67 GHz
				&& Max Memory Bandwidth : 32 GB/s
				&& SIMD support: SSE4.2
		& 24GB memory		
		& 2 nVidia Tesla M2090 `Fermi' GPU cards
				&& thread processors : 512
				&& peak double precision performance : 665 Gflops
				&& peak single precision performance : 1331 Gflops
				&& memory bandwidth : 177 GB/s
		& GPU accelerated
	\end{easylist}

\paragraph{Software}
	Since cuda-4.2 is not compatible with gcc 4.8.1, we choose gcc 4.4.7 as a substitute.
	\begin{easylist}[itemize]
	\ListProperties(Style2*=---)
		& gcc 4.4.7
		& icc 12.1.3
		& cuda-4.2
	\end{easylist}
	
%------------------------------------------------

\section{CPU optimization }
\subsection{Single CPU}
	\paragraph {Naive Implementation}
		The algorithm is translated into C literally. From complexity analysis above, we know the number of 
		float operations is $O(L)$, so we expect the execution time is proportional to $L$ and the performance
		keeps constant. Fig \ref{fig:naive} and Table \ref{tab:naive} confirms our expectation.

		\begin{figure}[H]
		\centering
		\captionsetup{width=0.6\textwidth}
		\includegraphics[width=0.6\textwidth]{./data/roofline.pdf}
		\caption{Roofline model for one Xeon X5650 kernel.}
		\label{fig:roofline}
		\end{figure}


		Intel Xeon X5650 can execute 4 float instructions per cycle, so the maximal float operation for each
		kernel per second is $4*clock speed=10.68 GFLOP/s$; on the other hand, the maximal memory bandwidth
		is $32GB/s$, so the turning point of \textbf{roofline model} occurs at $10.68/32=0.33375FLOPs/Byte$ as
		shown in Fig \ref{fig:roofline}.
		From Algorithm \ref{naivecode}, the inner loop loads 60 elements and takes 60 times of addition, 60
		times of subtraction, 60 times of multiplication and 2 times square root. Assume square root operation
		take 10 cycles to converge, then the corresponding computation intensity is
		$(60+60+60+2*4*10)/(4*60)=1.083$. Therefore the performance is mainly constricted by the clock speed of
		CPU, and the best estimated performance is coarsely $10.68/(1.083*4)=2.46\ elements/second$.

		However, the naive implementation exhibits less optimistic performance than the estimated one 
		in Table \ref{tab:naive}. The reason, as far as I'm concerned, lies in the optimal assumptions in the model.
		Anyway, this model gives us some intuition of bottleneck of Algorithm \ref{naivecode}.

		\begin{table}[H]
	 	\centering
		\rowcolors{1}{green}{pink}
	 	\begin{tabular}{c || c | c | c }
	 	\hline
		 L & 10240 & 20480 & 40960  \\
		 time &    0.577 &  1.153 &  2.3099  \\
		 performance &    0.545  & 0.546  & 0.545  \\

	    	\hline
	     	\end{tabular}
		\captionsetup{width=0.6\textwidth}
	      	\caption{Naive implementation with different $L$. The unit of time is second and the 
		unit of performance is billion/second. Code is compiled
		with gcc.
		}
	      	\label{tab:naive}
		\end{table}

		\begin{figure}[H]
		\centering
		\captionsetup{width=0.6\textwidth}
		\includegraphics[width=0.6\textwidth]{./data/naive.pdf}
		\caption{Execution time and performance of the Naive implementation. Time is measured 
		with unit second, and the unit of performance is billion elements per second.}
		\label{fig:naive}
		\end{figure}


	
	\paragraph{Simple SIMD}
		In this level of optimization, the part of calculating distance between two vectors are parallelized by
		SSE command. Since each vector is composed of 30 single precision numbers, the optimal speedup should approach
		4. In table \ref{tab:cpu}, we can see that the performance of \textit{gcc} and \textit{icc} is similar in this
		level, but 
		the speedup is about $2X$ for \textit{gcc} and $1.3X$ for \textit{icc}, since when implemented naively, 
		\textit{icc} gains more benefit from $-O3$ compiler flag.

		We expect the speedup would not be as large as 4 because of the ``roofline model''. The number of float
		 operations decreases approximately by a factor 4 by use of SSE instructions; as a result, 
		 the computation intensity will decrease roughly by the same factor.
		 Therefore the intensity will move to the left part of in Fig \ref{fig:roofline} , whose performance is restricted
		 by memory bandwidth. 
		So, after applying SIMD technique, we find that the bottleneck changes to be memory bandwidth, and thus further optimization
		should concentrate on memory usage. 
		

	\paragraph{Unrolled SIMD}
		Every time the vector distance is calculated, the first vector in each sequence is loaded; therefore, the first
		vector has been loaded for $L$ times in each sequence. In order to reduce the redundant loading cost, the
		middle loop in Algorithm \ref{naivecode} is unrolled by a factor 2 and 4. The performance is shown in Table \ref{tab:cpu}.

		For \textit{gcc}, the performance gains a little by unrolling the middle loop by a factor 2; while the effect backfires when
		the loop is unrolled by a factor 4. For \textit{icc}, both of these two implementation backfire.

	\begin{table}[H]
	 	\centering
		\rowcolors{1}{green}{pink}
	 	\begin{tabular}{c || c | c | c | c }
	 	\hline
		 & exploreNaive & exploreSSE & exploreSSEx2 & exploreSSEx4 \\
		gcc &    0.545 &  1.033 &  1.056 &  0.737 \\
		icc &    0.875  & 1.118  & 0.997  & 0.896 \\
		gcc+openmp &     2.200  &  2.996  & 3.085 &  2.769 \\
		icc+openmp  &    2.641  & 2.950  & 3.452  & 2.673  \\
		icc+cilk    &    3.797 &  3.751  & 3.792  & 3.786 \\

	    	\hline
	     	\end{tabular}
	      	\caption{ various kinds of optimization with different compilers. $L$ is fixed at 10240.}
	      	\label{tab:cpu}
	\end{table}



	\begin{figure}[H]
	\centering
	\captionsetup{width=0.8\textwidth}
	\includegraphics[width=0.8\textwidth]{./data/cpu.pdf}
	\caption{Single and multiple CPUs of optimization with different compilers. $L$ is fixed at 10240.
		The unit of y-axis is billion elements per second.}
	\label{xiong_upo1025_a3real}
	\end{figure}

\subsection{Multiple CPUs}
	\paragraph{Pre-analysis}
	From the analysis of parallelism in the introduction section, we know \textbf{Parallelism} is  
	\begin{equation}
	P=min(10\cdot Nb \cdot L,\ nT)
	\end{equation}
	where $nT$ is the number of computing threads. Since $L$ is much large than $nT=24$ in this case, the parallelism is 24. 
	However, from the result in table \ref{tab:cpu}, we can see that the actual parallelism is far less than 24. Both Intel
	Cilk and OpenMP are used to optimize the code. For OpenMP, the performance of \textit{gcc} and \textit{icc} is 
	compared, but for Intel Cilk, only the performance of \textit{icc} is tested. 
	
	\paragraph{Implementation details}
	The basic algorithm has three loops and the inner loop has an operation of summation, so I apply reduced parallelism in the 
	inner loop in order to avoid data racing. However, the reduced versions of OpenMP and Intel Cilk have a terrible performance;
	so I only parallelize the outer loop and middle loop.
	For SSEx2 SSEx4, only the outer loop is parallelized because the performance is bad if the middle loop is also parallelized.
	For the OpenMP version, the performance is not stable, so I took average of 10 spices.

	\paragraph{Result interpretation}
	For the $gcc+openmp$ combination, the speedup is between 3 and 4 for the four implementations; meanwhile, the 
	$icc+openmp$ has a similar performance and a little lower speedup because its single CPU performance is better
	than \textit{gcc}. Note that, in this case, the \textit{exploreSSEX2} has a better performance than the unrolled
	one \textit{exploreSSE}.
	\textit{icc+Cilk} is almost the same for all implementations which means the bottleneck now is the degree of parallelism not the 
	optimization of the execution part in each CPU. Also, it seems that the combination \textit{icc+Cilk} gives the best 
	performance of all implementations. 
	
	So, until now, the best speedup I can achieve on CPUs is $\frac{3.786}{0.545}=6.95$.





%------------------------------------------------

\section{GPU optimization}

\subsection{Methods based on shared memory usage}
	
\paragraph{Naive Implementation : cudaEx}
	cudaEx() in file ``gpuexplore.cu'' is the most straight-forward GPU implementation of Algorithm \ref{naivecode}.
	The block size is chosen as $30\times 4$, $30\times8$ or $30\times 16$ to investigate its impact on performance.

	\begin{table}[H]
		\captionsetup{width=0.6\textwidth}
	      	\caption{ blockIdx (0,0)	}
	 	\centering
		\rowcolors{1}{green}{green}
	 	\begin{tabularx}{0.35\textwidth}{| X | X | X | X | X |}
	 	\hline
		 $v_{0}(0)$ & $v_{0}(1)$ & $v_{0}(1)$ & $\cdots$ & $v_{0}(29)$  \\
	 	\hline
		 $v_{1}(0)$  & $v_{1}(1)$ & $v_{1}(2)$ & $\cdots$ & $v_{1}(29)$  \\
	 	\hline
		 $v_{2}(0)$  & $v_{2}(1)$ & $v_{2}(2)$ & $\cdots$ & $v_{2}(29)$  \\
	 	\hline
		 $\cdots$ & $\cdots$ & $\cdots$ & $\cdots$ & $\cdots$ \\
	 	\hline
		 $v_{7}(0)$ & $v_{7}(1)$ & $v_{7}(2)$ & $\cdots$ & $v_{7}(29)$  \\
	    	\hline
	     	\end{tabularx}
	      	\label{tab:block}
	\end{table}
	
	Table \ref{tab:block} gives one block configuration when block size is $30\times 8$ and the 
	gird size is $Nb\times L/8$ in this case. In such an implementation, 
	each row in Table \ref{tab:block} will load two vectors: the vector corresponding to the blockId and the first
	vector in this sequence, so each thread will load two elements.  
	\begin{table}[H]
	 	\centering
		\rowcolors{1}{cyan}{yellow}
	 	\begin{tabular}{c || c | c | c }
	 	\hline
		 block size & (30,16) & (30,8) & (30,4)  \\
		 register usage &   10 &  10 &  10  \\
		 shared memory &  3840  & 1920  & 960  \\
		 performance & 2.176	& 3.148	& 3.545 \\
	    	\hline
	     	\end{tabular}
		\captionsetup{width=0.6\textwidth}
	      	\caption{Naive implementation with different block sizes. The unit of shared memory is bytes and the 
		unit of performance is billion/second. Code is compiled
		with cuda-4.2.
		}
	      	\label{tab:cudaex}
	\end{table}
	Tesla M2090 has a memory bandwidth 177GB/s and its peak single precision performance is 1331 Gflops. In this case,
	the turning point in the ``roofline'' model is $1331/177=7.52 FLOP/s$. Since the computation intensity 1.083 
	is much smaller than the turning point, the program is memory bounded. The optimal performance should be
	$177/4=44.25\ elements/second$. The performance in Table \ref{tab:cudaex} can by no means match this number as a result
	of non-consecutive memory loading process.

\paragraph{Coalesced Implementation : cudaExCoalesce}
	In the naive implementation, each thread loads two vectors: the one corresponding to its thread Id and the first
	vector in each sequence, which results in three problems. First, it wastes
	bandwidth to load the first vector; secondly, it leads to the non-consecutive accessing of memory. Last, it gives
	birth to bank conflict because every thread in each block will try to load the first vector simultaneously.

	\begin{table}[H]
		\captionsetup{width=0.6\textwidth}
	      	\caption{ blockIdx (0,0)	}
	 	\centering
		\rowcolors{1}{green}{green}
	 	\begin{tabularx}{0.35\textwidth}{| X | X | X | X | X |}
	 	\hline
		 $v_{0}(0)$ & $v_{0}(1)$ & $v_{0}(1)$ & $\cdots$ & $v_{0}(29)$  \\
	 	\hline
		 $v_{1}(0)$  & $v_{1}(1)$ & $v_{1}(2)$ & $\cdots$ & $v_{1}(29)$  \\
	 	\hline
		 $v_{2}(0)$  & $v_{2}(1)$ & $v_{2}(2)$ & $\cdots$ & $v_{2}(29)$  \\
	 	\hline
		 $\cdots$ & $\cdots$ & $\cdots$ & $\cdots$ & $\cdots$ \\
	 	\hline
		 $v_{7}(0)$ & $v_{7}(1)$ & $v_{7}(2)$ & $\cdots$ & $v_{7}(29)$  \\
	 	\hline
		 $v_{0}(0)$ & $v_{0}(1)$ & $v_{0}(1)$ & $\cdots$ & $v_{0}(29)$  \\
	    	\hline
	     	\end{tabularx}
	      	\label{tab:blockcoal}
	\end{table}
	
	A partial solution is to add a row in the shared memory to store the first vector as shown in Table 
	\ref{tab:blockcoal}; in such away, 
	each block only needs to load the first vector once and all the operations can be done within the
	shared memory. Function ``cudaExCoalesce()'' in file ``gpuexplore.cu'' implements this idea and 
	the performance is shown in Table \ref{tab:cudamem}. It can be seen that there is a $1.25$ times
	speedup for $(30,16)$ and a small speedup for block size $(30,8)$; while it backfires when block
	size is as small as $(30,4)$.

\paragraph{Perform one operation when loading element : cudaExCoalesceSub}
	The performance can be improved further if we perform the subtraction and addition operation when
	threads load elements, but we should not expect to gain too much from such a small modification 
	because the number of operations, which load all the elements, does not decrease at all. Table
	\ref{tab:cudamem} confirms our claim.

	\begin{table}[H]
	 	\centering
		\rowcolors{1}{cyan}{yellow}
	 	\begin{tabular}{c || c | c | c }
	 	\hline
		 block size & (30,16) & (30,8) & (30,4)  \\
		 register usage &   11 &  11 &  11  \\
		 shared memory &  3960  & 2040  & 1080  \\
		 cudaExCoalesce	& 2.724	& 3.350	& 3.372	\\
		 cudaExCoalesceSub	& 2.759	& 3.388	& 3.386	\\
		 cudaExCoalesceSubUnsync	& 2.918	& 3.452	& 3.497 \\
	    	\hline
	     	\end{tabular}
		\captionsetup{width=0.8\textwidth}
	      	\caption{Improved implementations based on the naive one. The unit of shared memory is bytes and the 
		unit of peformance is billion/second. Code is compiled
		with cuda-4.2.
		}
	      	\label{tab:cudamem}
	\end{table}

	\begin{figure}[H]
	\centering
	\captionsetup{width=0.6\textwidth}
	\includegraphics[width=0.8\textwidth]{./data/gpu.pdf}
	\caption{Various implementation of GPU code with three kinds of block sizes.}
	\label{fig:cudaex}
	\end{figure}


\paragraph{Unsynchronized implementation : cudaExCoalesceSubUnsync}
	The inner loop in Algorithm \ref{naivecode} performs addition of 30 elements twice. When implemented in
	the naive code, I just follow the reduction summation method in our assignment \textit{lab07}. However,
	the reduction summation algorithm employs ``\_\_syncthreads()'' operation frequently, so the performance
	will improve if these synchronizing processes are eliminated or reduced. Function
	``cudaExCoalesceSubUnsync'' in file ``gpuexplore.cu'' implements the unsynchronized method, and the performance
	is documented in Table \ref{tab:cudamem}. We find that the speedup is almost negligible compared with
	``cudaExCoalesceSub'', which pushes me to optimize the code in different aspect. 
	

\subsection{Methods based on register usage}
	In Table \ref{tab:cudaex} and \ref{tab:cudamem}, we find that the usage of shared memory is heavy, but most of 
	the registers in each thread are idling.  As was pointed out in previous section, the bottleneck lies in the
	memory bandwidth. Since the bandwidth of registers is much larger  than that of shared memory, the 
	performance will boost if the code has a better usage of registers. 

\paragraph{Single vector each thread : cudaSingle}
	In this implementation, each thread loads a whole vector and calculate the distance between this vector and 
	the first vector, so the computation load of each thread is heavy, and the number of registers used by a single
	thread increases as is documented in Table \ref{tab:single}.

	The total number of threads decreases as each thread load 30 elements now, and the size of each block has squeezed
	by a factor 30 as shown in Table \ref{tab:single}. Function ``cudaSingle()'' in file ``gpuexplore.cu'' implements
	this method, and the performance for block size $(1,16)$ and $(1,8)$ has
	improved compared with ``cudaExCoalesceSubUnsync'' for block size $(30,16)$ and $(30,8)$; however,
	this implementation backfires for block size $(1,4)$.
	
	\begin{table}[H]
	 	\centering
		\rowcolors{1}{cyan}{yellow}
	 	\begin{tabular}{c || c | c | c | c }
	 	\hline
		 block size & (1,32) & (1,16) & (1,8) & (1,4)  \\
		 register usage &   22 &  22 &  22	& 22  \\
		 shared memory &  120  & 120  & 120 & 120  \\
		 cudaSingle & 3.929	& 5.542	& 4.500	& 3.050\\
		 cudaExSingleX2	& 2.295	& 3.634	& 5.024 & 3.955	\\
	    	\hline
	     	\end{tabular}
		\captionsetup{width=0.8\textwidth}
	      	\caption{performance of cudaSingle() and cudaSingleX2(). The unit of shared memory is bytes and the 
		unit of performance is billion/second. Code is compiled
		with cuda-4.2.
		}
	      	\label{tab:single}
	\end{table}

\paragraph{Two vectors each thread : cudaSingleX2}
	We can extend ``cudaSingle'' to ``cudaSingleX2'' in which each thread loads two vectors, so the computation
	load doubles and the total number of threads halves. The comparison of ``cudaSingle'' and ``cudaSingleX2''
	is drew in Fig \ref{fig:cudasingle}. ``cudaSingleX2'' has an improvement when block size is $(1,8)$ and
	$(1,4)$, but is inferior to ``cudaSingle'' when block size is $(1,32)$ or $(1,16)$.

	\begin{figure}[H]
	\centering
	\captionsetup{width=0.6\textwidth}
	\includegraphics[width=0.6\textwidth]{./data/single.pdf}
	\caption{performance of cudaSingle() and cudaSingleX2() for different block sizes. }
	\label{fig:cudasingle}
	\end{figure}

	The best speedup in the GPU version of Algorithm \ref{naivecode} until now is $5.542/0.545=10.1688$.

%------------------------------------------------

\section{Conclusion}

	CPU and GPU optimization has been implemented for Algorithm \ref{naivecode}. SIMD and multi thread techniques are used in 
	CPU optimization and the analysis is based on ``roofline model'' and memory usage. Multi CPU optimization \textit{icc+Cilk}
	has the best performance of all the CPU implementations and the maximal speedup is \textbf{6.95}x.
	
	Shared Memory usage and register usage are taken into account in the GPU optimization. Performance of six different
	implementations is compared, and we find Register usage plays
	a more important role than shared memory usage. The ``cudaSingle'' implementation with block size $(1,16)$ has
	the best performance with speedup \textbf{10.17}x.
	

%-------------------------------------------------
\section*{Acknowledgments} % The \section*{} command stops section numbering

\addcontentsline{toc}{section}{\hspace*{-\tocsep}Acknowledgments} % Adds this section to the table of contents with negative horizontal space equal to the indent for the numbered sections

As a physic major, I benefit a lot from this course since my research relies heavily on matrix computation, which is expensive 
occasionally. 
Thanks for Prof.Richard Vuduc, TA Marat Dukhan and TA Jee Choi for their excellent lectures and patience to answer questions
in Piassa.


%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------

\bibliographystyle{unsrt}
\bibliography{report}

%----------------------------------------------------------------------------------------

\end{document}
