#include "preexplore.h"
#include <x86intrin.h>

#ifdef CILK
	#include <cilk/reducer_opadd.h>
#endif

#ifdef OPENMP
	#include <omp.h>
#endif

/*
Description of variables:
	N2: the dimension of vectors.
	L: the number of vectors generated from a single seed.
	nb: the number of different seeds.
	minv: store the index of vector which has the shortest distance.
*/

/* the Naive implementation on a single CPU, which is also used to verify other implementation.
*/
void
exploreNaive(float *a, float *dif1, float *dif2, int *label, int L, int nb){
#ifdef CILK
	_Cilk_for(size_t i=0; i<nb; i++){	
#else
	#ifdef OPENMP
		#pragma omp parallel for default(none) shared(a, dif1, dif2,label, L, nb)
	#endif
	for(size_t i=0; i<nb; i++){
#endif
		#ifdef CILK
		_Cilk_for(size_t j=0; j<L; j++){
		#else
			#ifdef OPENMP
				#pragma omp parallel for default(none) shared(a, dif1, dif2,label, L, nb,i)
			#endif
		for(size_t j=0; j<L; j++){
		#endif
			/*
			#ifdef CILK
			cilk::reducer_opadd<float> s1(0), s2(0);
			_Cilk_for(size_t k=0; k<N2; k++){
				s1+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s2+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
			}
			dif1[i*L+j]=sqrt(s1.get_value());
			dif2[i*L+j]=sqrt(s2.get_value());
			#else
			*/
			float s1=0.0;
			float s2=0.0;
			#ifdef OPENMP
			//	#pragma omp parallel for reduction (+:s1, s2)
			//	coment out the reducer for its bad performance.
			#endif
			for(size_t k=0; k<N2; k++){
				s1+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s2+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
			}
			dif1[i*L+j]=sqrt(s1);
			dif2[i*L+j]=sqrt(s2);
			//#endif

			if(dif1[i*L+j]< tol && dif2[i*L+j]< tol) label[i*L+j]=0;
			else if(dif1[i*L+j]< tol && dif2[i*L+j]> tol) label[i*L+j]=1;
			else if(dif1[i*L+j]> tol && dif2[i*L+j]< tol) label[i*L+j]=2;
			else  label[i*L+j]=3;
		}
		
	}

}

/* The SSE version on a single CPU. Address is not aligned.
   The 'label' part keeps the same for better use of cache. 
*/
void
exploreSSE(float *a, float *dif1,  float *dif2, int *label, int L, int nb){
#ifdef CILK
	_Cilk_for(size_t i=0; i<nb; i++){	
#else
	#ifdef OPENMP
		#pragma omp parallel for default(none) shared(a, dif1, dif2,label, L, nb)
	#endif
	for(size_t i=0; i<nb; i++){
#endif
		#ifdef CILK
		_Cilk_for(size_t j=0; j<L; j++){
		#else
			#ifdef OPENMP
				#pragma omp parallel for default(none) shared(a, dif1, dif2,label, L, nb,i)
			#endif
		for(size_t j=0; j<L; j++){
		#endif
			__m128 sum1=_mm_setzero_ps();
			__m128 sum2=_mm_setzero_ps();
			__m128 sub,sub1,sub2,lo,hi;
			size_t k=0;
			for( ; k<N2-3; k+=4){
				sub1=_mm_loadu_ps(&a[i*N2*L+j*N2+k]);
				sub2=_mm_loadu_ps(&a[i*N2*L+k]);
				sub=_mm_sub_ps(sub1,sub2);
				sum1=_mm_add_ps(sum1,_mm_mul_ps(sub,sub));	
				sub=_mm_add_ps(sub1,sub2);
				sum2=_mm_add_ps(sum2,_mm_mul_ps(sub,sub));	
			}
			/* the following tries to reuse the register names*/
                        lo=_mm_unpacklo_ps(sum1,sum1);
                        hi=_mm_unpackhi_ps(sum1,sum1);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum1=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum1=_mm_add_ps(sum1,sub);

                        lo=_mm_unpacklo_ps(sum2,sum2);
                        hi=_mm_unpackhi_ps(sum2,sum2);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum2=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum2=_mm_add_ps(sum2,sub);

			float s1=_mm_cvtss_f32(sum1);
			float s2=_mm_cvtss_f32(sum2);
			for( ; k<N2; k++){
				s1+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s2+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
			}
			
			dif1[i*L+j]=sqrt(s1);
			dif2[i*L+j]=sqrt(s2);

			if(dif1[i*L+j]< tol && dif2[i*L+j]< tol) label[i*L+j]=0;
			else if(dif1[i*L+j]< tol && dif2[i*L+j]> tol) label[i*L+j]=1;
			else if(dif1[i*L+j]> tol && dif2[i*L+j]< tol) label[i*L+j]=2;
			else  label[i*L+j]=3;
		}
	}

}

void
exploreSSEx2(float *a, float *dif1, float *dif2, int *label,  int L, int nb){
#ifdef CILK
	_Cilk_for(size_t i=0; i<nb; i++){	
#else
	#ifdef OPENMP
		#pragma omp parallel for default(none) shared(a, dif1, dif2,label, L, nb)
	#endif
	for(size_t i=0; i<nb; i++){
#endif
		size_t j;
		for(j=0; j<L-1; j+=2){
			__m128 sum0=_mm_setzero_ps();
			__m128 sum1=_mm_setzero_ps();
			__m128 sum2=_mm_setzero_ps();
			__m128 sum3=_mm_setzero_ps();

			__m128 sub,sub1,lo,hi,start;
			size_t k=0;
			for( ; k<N2-3; k+=4){
				start=_mm_loadu_ps(&a[i*N2*L+k]);
				
				sub1=_mm_loadu_ps(&a[i*N2*L+j*N2+k]);
				sub=_mm_sub_ps(sub1, start );
				sum0=_mm_add_ps(sum0,_mm_mul_ps(sub,sub));	
				sub=_mm_add_ps(sub1, start );
				sum1=_mm_add_ps(sum1,_mm_mul_ps(sub,sub));	

				sub1=_mm_loadu_ps(&a[i*N2*L+(j+1)*N2+k]);
				sub=_mm_sub_ps(sub1, start );
				sum2=_mm_add_ps(sum2,_mm_mul_ps(sub,sub));	
				sub=_mm_add_ps(sub1, start );
				sum3=_mm_add_ps(sum3,_mm_mul_ps(sub,sub));	
				
			}
			/* the following tries to reuse the register names*/
			float s[4];
			lo=_mm_unpacklo_ps(sum0,sum0);
			hi=_mm_unpackhi_ps(sum0,sum0);
			sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
			sum0=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
			sum0=_mm_add_ps(sum0,sub);
			s[0]=_mm_cvtss_f32(sum0);

			lo=_mm_unpacklo_ps(sum1,sum1);
			hi=_mm_unpackhi_ps(sum1,sum1);
			sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
			sum1=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
			sum1=_mm_add_ps(sum1,sub);
			s[1]=_mm_cvtss_f32(sum1);

			lo=_mm_unpacklo_ps(sum2,sum2);
			hi=_mm_unpackhi_ps(sum2,sum2);
			sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
			sum2=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
			sum2=_mm_add_ps(sum2,sub);
			s[2]=_mm_cvtss_f32(sum2);

			lo=_mm_unpacklo_ps(sum3,sum3);
			hi=_mm_unpackhi_ps(sum3,sum3);
			sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
			sum3=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
			sum3=_mm_add_ps(sum3,sub);
			s[3]=_mm_cvtss_f32(sum3);
			
			
			
			for( ; k<N2; k++){
				s[0]+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s[1]+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
				s[2]+=(a[i*N2*L+(j+1)*N2+k]-a[i*N2*L+k])*(a[i*N2*L+(j+1)*N2+k]-a[i*N2*L+k]);
				s[3]+=(a[i*N2*L+(j+1)*N2+k]+a[i*N2*L+k])*(a[i*N2*L+(j+1)*N2+k]+a[i*N2*L+k]);
			}
			
			dif1[i*L+j]=sqrt(s[0]);
			dif2[i*L+j]=sqrt(s[1]);
			dif1[i*L+j+1]=sqrt(s[2]);
			dif2[i*L+j+1]=sqrt(s[3]);

			if(dif1[i*L+j]< tol && dif2[i*L+j]< tol) label[i*L+j]=0;
			else if(dif1[i*L+j]< tol && dif2[i*L+j]> tol) label[i*L+j]=1;
			else if(dif1[i*L+j]> tol && dif2[i*L+j]< tol) label[i*L+j]=2;
			else  label[i*L+j]=3;

			if(dif1[i*L+j+1]< tol && dif2[i*L+j+1]< tol) label[i*L+j+1]=0;
			else if(dif1[i*L+j+1]< tol && dif2[i*L+j+1]> tol) label[i*L+j+1]=1;
			else if(dif1[i*L+j+1]> tol && dif2[i*L+j+1]< tol) label[i*L+j+1]=2;
			else  label[i*L+j+1]=3;
		}

		for( ; j<L; j++){

			for(size_t j=0; j<L; j++){
			__m128 sum1=_mm_setzero_ps();
			__m128 sum2=_mm_setzero_ps();
			__m128 sub,sub1,sub2,lo,hi;
			size_t k=0;
			for( ; k<N2-3; k+=4){
				sub1=_mm_loadu_ps(&a[i*N2*L+j*N2+k]);
				sub2=_mm_loadu_ps(&a[i*N2*L+k]);
				sub=_mm_sub_ps(sub1,sub2);
				sum1=_mm_add_ps(sum1,_mm_mul_ps(sub,sub));	
				sub=_mm_add_ps(sub1,sub2);
				sum2=_mm_add_ps(sum2,_mm_mul_ps(sub,sub));	
			}
			/* the following tries to reuse the register names*/
                        lo=_mm_unpacklo_ps(sum1,sum1);
                        hi=_mm_unpackhi_ps(sum1,sum1);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum1=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum1=_mm_add_ps(sum1,sub);

                        lo=_mm_unpacklo_ps(sum2,sum2);
                        hi=_mm_unpackhi_ps(sum2,sum2);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum2=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum2=_mm_add_ps(sum2,sub);

			float s1=_mm_cvtss_f32(sum1);
			float s2=_mm_cvtss_f32(sum2);
			for( ; k<N2; k++){
				s1+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s2+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
			}
			
			dif1[i*L+j]=sqrt(s1);
			dif2[i*L+j]=sqrt(s2);

			if(dif1[i*L+j]< tol && dif2[i*L+j]< tol) label[i*L+j]=0;
			else if(dif1[i*L+j]< tol && dif2[i*L+j]> tol) label[i*L+j]=1;
			else if(dif1[i*L+j]> tol && dif2[i*L+j]< tol) label[i*L+j]=2;
			else  label[i*L+j]=3;
	
			}

		}
	}
}


/* The SSE version on a single CPU. data reused by a factor 4. since every time I need to load the
first vector, so exploreSSEx4() will promote the performance, but not that much.
*/

void
exploreSSExx(float *a, float *dif1, float *dif2, int *label,  int L, int nb, const int p){
	//const int p=4;
#ifdef CILK
	_Cilk_for(size_t i=0; i<nb; i++){	
#else
	#ifdef OPENMP
		#pragma omp parallel for default(none) shared(a, dif1, dif2,label, L, nb)
	#endif
	for(size_t i=0; i<nb; i++){
#endif
		size_t j=0;
		for(; j<L-p+1; j+=p){
			__m128 sum[2*p];
			for(size_t n=0; n<2*p; n++) sum[n]=_mm_setzero_ps();

			__m128 sub,sub1,lo,hi,start;
			size_t k=0;
			for( ; k<N2-3; k+=4){
				start=_mm_loadu_ps(&a[i*N2*L+k]);
				for(size_t n=0;n<p; n++){
					sub1=_mm_loadu_ps(&a[i*N2*L+(j+n)*N2+k]);
					sub=_mm_sub_ps(sub1, start );
					sum[2*n]=_mm_add_ps(sum[2*n],_mm_mul_ps(sub,sub));	
					sub=_mm_add_ps(sub1, start );
					sum[2*n+1]=_mm_add_ps(sum[2*n+1],_mm_mul_ps(sub,sub));	
				}
				
			}
			/* the following tries to reuse the register names*/
			float s[2*p];
			for(size_t n=0; n<2*p; ++n){
				lo=_mm_unpacklo_ps(sum[n],sum[n]);
				hi=_mm_unpackhi_ps(sum[n],sum[n]);
				sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
				sum[n]=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
				sum[n]=_mm_add_ps(sum[n],sub);
				s[n]=_mm_cvtss_f32(sum[n]);
			}
			
			for( ; k<N2; k++){
				for(size_t n=0; n<p; n++){
					s[2*n]+=(a[i*N2*L+(j+n)*N2+k]-a[i*N2*L+k])*(a[i*N2*L+(j+n)*N2+k]-a[i*N2*L+k]);
					s[2*n+1]+=(a[i*N2*L+(j+n)*N2+k]+a[i*N2*L+k])*(a[i*N2*L+(j+n)*N2+k]+a[i*N2*L+k]);
				}
			}
			
			for(size_t n=0; n<p; n++){
				dif1[i*L+j+n]=sqrt(s[2*n]);
				dif2[i*L+j+n]=sqrt(s[2*n+1]);

				if(dif1[i*L+j+n]< tol && dif2[i*L+j+n]< tol) label[i*L+j+n]=0;
				else if(dif1[i*L+j+n]< tol && dif2[i*L+j+n]> tol) label[i*L+j+n]=1;
				else if(dif1[i*L+j+n]> tol && dif2[i*L+j+n]< tol) label[i*L+j+n]=2;
				else  label[i*L+j+n]=3;
			}
		}

		for( ; j<L; j++){

			for(size_t j=0; j<L; j++){
			__m128 sum1=_mm_setzero_ps();
			__m128 sum2=_mm_setzero_ps();
			__m128 sub,sub1,sub2,lo,hi;
			size_t k=0;
			for( ; k<N2-3; k+=4){
				sub1=_mm_loadu_ps(&a[i*N2*L+j*N2+k]);
				sub2=_mm_loadu_ps(&a[i*N2*L+k]);
				sub=_mm_sub_ps(sub1,sub2);
				sum1=_mm_add_ps(sum1,_mm_mul_ps(sub,sub));	
				sub=_mm_add_ps(sub1,sub2);
				sum2=_mm_add_ps(sum2,_mm_mul_ps(sub,sub));	
			}
			/* the following tries to reuse the register names*/
                        lo=_mm_unpacklo_ps(sum1,sum1);
                        hi=_mm_unpackhi_ps(sum1,sum1);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum1=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum1=_mm_add_ps(sum1,sub);

                        lo=_mm_unpacklo_ps(sum2,sum2);
                        hi=_mm_unpackhi_ps(sum2,sum2);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum2=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum2=_mm_add_ps(sum2,sub);

			float s1=_mm_cvtss_f32(sum1);
			float s2=_mm_cvtss_f32(sum2);
			for( ; k<N2; k++){
				s1+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s2+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
			}
			
			dif1[i*L+j]=sqrt(s1);
			dif2[i*L+j]=sqrt(s2);

			if(dif1[i*L+j]< tol && dif2[i*L+j]< tol) label[i*L+j]=0;
			else if(dif1[i*L+j]< tol && dif2[i*L+j]> tol) label[i*L+j]=1;
			else if(dif1[i*L+j]> tol && dif2[i*L+j]< tol) label[i*L+j]=2;
			else  label[i*L+j]=3;
	
			}

		}
	}
}

/* cpuExplore() is the envirement to test the code in cpu.
*/
void
cpuExplore(float *a, float *dif1, float *dif2, int *label, int L, int nb, int mode){
	/*definition of time variable*/
	struct stopwatch_t* timer = NULL;
	long double t_cpu;
        /* Setup timers */
        stopwatch_init ();
        timer = stopwatch_create ();
	
	stopwatch_start (timer);
	switch(mode){
		case 1 : exploreNaive(a,dif1,dif2,label,L,nb); break;
		case 2 : exploreSSE(a,dif1,dif2,label,L,nb); break;
		case 3 : exploreSSEx2(a,dif1,dif2,label,L,nb); break;
		case 4 : exploreSSExx(a,dif1,dif2,label,L,nb,4); break;
		default: 
			 exploreNaive(a,dif1,dif2,label,L,nb); break;

	}
	t_cpu = stopwatch_stop (timer);
	
	fprintf (stderr, "CPU distance %d : %Lg secs ==> %Lg billion elements/second\n", mode, t_cpu, (N2*L*nb) / t_cpu * 1e-9 );
	
}

int
cmp(float *a1, float *a2, float *b1, float *b2, int *c, int *d, int N){
	int s1=0;
	int s2=0;
	int s3=0;
	for(int i=0; i<N; i++){
		if(abs(a1[i]-a2[i]>1e-5)){ 
			s1++;
			fprintf(stderr,"%d\t%f\t%f\n", i,a1[i],a2[i]);
		}
	}
	for(int i=0; i<N; i++){
		if(abs(b1[i]-b2[i]>1e-5)){ 
			s2++;
			fprintf(stderr,"%d\t%f\t%f\n", i,b1[i],b2[i]);
		}
	}
	for(int i=0; i<N; i++){
		if(c[i]!=d[i]){
			s3++;
			fprintf(stderr, "%d\t%d\t%d\n", i,c[i],d[i]);
		}
	}
	
	return s1+s2+s3;
}

