#ifndef CMATH_H
	#define CMATH_H
	#include <math.h>
#endif

#ifndef CSTDLIB_H
	#define CSTDLIB_H
	#include <stdlib.h>
#endif

#ifndef CSTDIO_H
	#define CSTDIO_H
	#include <stdio.h>
#endif

#ifndef CONST_H
	#define CONST_H
	#include "const.h"
#endif

#ifndef TIMER_H
	#define TIMER_H
	#include "timer.h"
#endif

#ifndef PREEXPLORE_H
	#define PREEXPLORE_H 
void
cpuExplore(float *a, float *dif1, float *dif2, int *label, int L, int nb, int mode);

void 
gpuExplore(float *a, float *dif1,  float *dif2, int *label,  int L, int nb, int mode);

int
cmp(float *a1, float *a2, float *b1, float *b2, int *c, int *d, int N);
#endif

//#define CILK
//#define OPENMP
