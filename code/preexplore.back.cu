#include "preexplore.h"
#include <x86intrin.h>
/*
Description of variables:
	N2: the dimension of vectors.
	L: the number of vectors generated from a single seed.
	nb: the number of different seeds.
	minv: store the index of vector which has the shortest distance.
*/

/* the Naive implementation on a single CPU, which is also used to verify other implementation.
*/
void
exploreNaive(float *a, float *dif1, float *dif2, int *label, int L, int nb){
	for(size_t i=0; i<nb; i++){
		for(size_t j=0; j<L; j++){
			float s1=0.0;
			float s2=0.0;
			for(size_t k=0; k<N2; k++){
				s1+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s2+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
			}
			dif1[i*L+j]=sqrt(s1);
			dif2[i*L+j]=sqrt(s2);
			if(dif1[i*L+j]< tol && dif2[i*L+j]< tol) label[i*L+j]=0;
			else if(dif1[i*L+j]< tol && dif2[i*L+j]> tol) label[i*L+j]=1;
			else if(dif1[i*L+j]> tol && dif2[i*L+j]< tol) label[i*L+j]=2;
			else  label[i*L+j]=3;
		}
		
	}

}

/* The SSE version on a single CPU. Address is not aligned.
   The 'label' part keeps the same for better use of cache. 
*/
void
exploreSSE(float *a, float *dif1,  float *dif2, int *label, int L, int nb){
	for(size_t i=0; i<nb; i++){
		for(size_t j=0; j<L; j++){
			__m128 sum1=_mm_setzero_ps();
			__m128 sum2=_mm_setzero_ps();
			__m128 sub,sub1,sub2,lo,hi;
			size_t k=0;
			for( ; k<N2-3; k+=4){
				sub1=_mm_loadu_ps(&a[i*N2*L+j*N2+k]);
				sub2=_mm_loadu_ps(&a[i*N2*L+k]);
				sub=_mm_sub_ps(sub1,sub2);
				sum1=_mm_add_ps(sum1,_mm_mul_ps(sub,sub));	
				sub=_mm_add_ps(sub1,sub2);
				sum2=_mm_add_ps(sum2,_mm_mul_ps(sub,sub));	
			}
			/* the following tries to reuse the register names*/
                        lo=_mm_unpacklo_ps(sum1,sum1);
                        hi=_mm_unpackhi_ps(sum1,sum1);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum1=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum1=_mm_add_ps(sum1,sub);

                        lo=_mm_unpacklo_ps(sum2,sum2);
                        hi=_mm_unpackhi_ps(sum2,sum2);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum2=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum2=_mm_add_ps(sum2,sub);

			float s1=_mm_cvtss_f32(sum1);
			float s2=_mm_cvtss_f32(sum2);
			for( ; k<N2; k++){
				s1+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s2+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
			}
			
			dif1[i*L+j]=sqrt(s1);
			dif2[i*L+j]=sqrt(s2);

			if(dif1[i*L+j]< tol && dif2[i*L+j]< tol) label[i*L+j]=0;
			else if(dif1[i*L+j]< tol && dif2[i*L+j]> tol) label[i*L+j]=1;
			else if(dif1[i*L+j]> tol && dif2[i*L+j]< tol) label[i*L+j]=2;
			else  label[i*L+j]=3;
		}
	}

}

/* The SSE version on a single CPU. data reused by a factor 4. since every time I need to load the
first vector, so exploreSSEx4() will promote the performance, but not that much.
*/
void
exploreSSEx4(float *a, float *dif1, float *dif2, int *label,  int L, int nb){
	for(size_t i=0; i<nb; i++){
		size_t j=0;
		for(; j<L-3; j+=4){
			__m128 sum[8];
			for(size_t n=0; n<8; n++) sum[n]=_mm_setzero_ps();

			__m128 sub,sub1,lo,hi,start;
			size_t k=0;
			for( ; k<N2-3; k+=4){
				start=_mm_loadu_ps(&a[i*N2*L+k]);
				
				for(size_t n=0;n<4; n++){
					sub1=_mm_loadu_ps(&a[i*N2*L+(j+n)*N2+k]);
					sub=_mm_sub_ps(sub1, start );
					sum[2*n]=_mm_add_ps(sum[2*n],_mm_mul_ps(sub,sub));	
					sub=_mm_add_ps(sub1, start );
					sum[2*n+1]=_mm_add_ps(sum[2*n+1],_mm_mul_ps(sub,sub));	
				}
				
			}
			/* the following tries to reuse the register names*/
			float s[8];
			for(size_t n=0; n<8; ++n){
				lo=_mm_unpacklo_ps(sum[n],sum[n]);
				hi=_mm_unpackhi_ps(sum[n],sum[n]);
				sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
				sum[n]=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
				sum[n]=_mm_add_ps(sum[n],sub);
				s[n]=_mm_cvtss_f32(sum[n]);
			}
			
			for( ; k<N2; k++){
				for(size_t n=0; n<4; n++){
					s[2*n]+=(a[i*N2*L+(j+n)*N2+k]-a[i*N2*L+k])*(a[i*N2*L+(j+n)*N2+k]-a[i*N2*L+k]);
					s[2*n+1]+=(a[i*N2*L+(j+n)*N2+k]+a[i*N2*L+k])*(a[i*N2*L+(j+n)*N2+k]+a[i*N2*L+k]);
				}
			}
			
			for(size_t n=0; n<4; n++){
				dif1[i*L+j+n]=sqrt(s[2*n]);
				dif2[i*L+j+n]=sqrt(s[2*n+1]);

				if(dif1[i*L+j+n]< tol && dif2[i*L+j+n]< tol) label[i*L+j+n]=0;
				else if(dif1[i*L+j+n]< tol && dif2[i*L+j+n]> tol) label[i*L+j+n]=1;
				else if(dif1[i*L+j+n]> tol && dif2[i*L+j+n]< tol) label[i*L+j+n]=2;
				else  label[i*L+j+n]=3;
			}
		}

		for( ; j<L; j++){

			for(size_t j=0; j<L; j++){
			__m128 sum1=_mm_setzero_ps();
			__m128 sum2=_mm_setzero_ps();
			__m128 sub,sub1,sub2,lo,hi;
			size_t k=0;
			for( ; k<N2-3; k+=4){
				sub1=_mm_loadu_ps(&a[i*N2*L+j*N2+k]);
				sub2=_mm_loadu_ps(&a[i*N2*L+k]);
				sub=_mm_sub_ps(sub1,sub2);
				sum1=_mm_add_ps(sum1,_mm_mul_ps(sub,sub));	
				sub=_mm_add_ps(sub1,sub2);
				sum2=_mm_add_ps(sum2,_mm_mul_ps(sub,sub));	
			}
			/* the following tries to reuse the register names*/
                        lo=_mm_unpacklo_ps(sum1,sum1);
                        hi=_mm_unpackhi_ps(sum1,sum1);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum1=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum1=_mm_add_ps(sum1,sub);

                        lo=_mm_unpacklo_ps(sum2,sum2);
                        hi=_mm_unpackhi_ps(sum2,sum2);
                 	sub=_mm_add_ps(_mm_unpacklo_ps(lo,lo),_mm_unpackhi_ps(lo,lo));  
                 	sum2=_mm_add_ps(_mm_unpacklo_ps(hi,hi),_mm_unpackhi_ps(hi,hi));  
                  	sum2=_mm_add_ps(sum2,sub);

			float s1=_mm_cvtss_f32(sum1);
			float s2=_mm_cvtss_f32(sum2);
			for( ; k<N2; k++){
				s1+=(a[i*N2*L+j*N2+k]-a[i*N2*L+k])*(a[i*N2*L+j*N2+k]-a[i*N2*L+k]);
				s2+=(a[i*N2*L+j*N2+k]+a[i*N2*L+k])*(a[i*N2*L+j*N2+k]+a[i*N2*L+k]);
			}
			
			dif1[i*L+j]=sqrt(s1);
			dif2[i*L+j]=sqrt(s2);

			if(dif1[i*L+j]< tol && dif2[i*L+j]< tol) label[i*L+j]=0;
			else if(dif1[i*L+j]< tol && dif2[i*L+j]> tol) label[i*L+j]=1;
			else if(dif1[i*L+j]> tol && dif2[i*L+j]< tol) label[i*L+j]=2;
			else  label[i*L+j]=3;
	
			}

		}
	}
}

/* cpuExplore() is the envirement to test the code in cpu.
*/
void
cpuExplore(float *a, float *dif1, float *dif2, int *label, int L, int nb, int mode){
	/*definition of time variable*/
	struct stopwatch_t* timer = NULL;
	long double t_cpu;
        /* Setup timers */
        stopwatch_init ();
        timer = stopwatch_create ();
	
	stopwatch_start (timer);
	switch(mode){
		case 1 : exploreNaive(a,dif1,dif2,label,L,nb); break;
		case 2 : exploreSSE(a,dif1,dif2,label,L,nb); break;
		case 3 : exploreSSEx4(a,dif1,dif2,label,L,nb); break;
		default: 
			 exploreNaive(a,dif1,dif2,label,L,nb); break;

	}
	t_cpu = stopwatch_stop (timer);
	
	fprintf (stderr, "CPU distance %d : %Lg secs ==> %Lg billion elements/second\n", mode, t_cpu, (N2*L*nb) / t_cpu * 1e-9 );
	
}
/*
__global__ void
cudaExCoalesceSubUnsyn(float *a, float *dif, int L, int nb){

	__shared__ float t[N2][BL+1];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	int tx=threadIdx.x;
	int ty=threadIdx.y;

	if(ty==0) t[tx][BL]=a[by*N2*L+tx];
	__syncthreads();
	t[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx]-t[tx][BL];
	t[tx][ty]*=t[tx][ty];
	__syncthreads();


	size_t stride=N2/2;

	if(tx<stride) t[tx][ty]+=t[tx+stride][ty];
	__syncthreads();
	
	stride=(stride+1)/2;
	if(tx>0 && tx<stride) t[tx][ty]+=t[tx+stride-1][ty];
	__syncthreads();
	
	for(stride/=2;stride>8; stride/=2){
		if(tx<stride) t[tx][ty]+=t[tx+stride][ty];
		__syncthreads();
	}
	

	if(tx==0) dif[by*L+bx*BL+ty]=sqrt(t[0][ty]+t[1][ty]+t[2][ty]+t[3][ty]
					+t[4][ty]+t[5][ty]+t[6][ty]+t[7][ty]); 
}


__global__ void
cudaExCoalesceSub(float *a, float *dif, int L, int nb){

	__shared__ float t[N2][BL+1];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	int tx=threadIdx.x;
	int ty=threadIdx.y;

	if(ty==0) t[tx][BL]=a[by*N2*L+tx];
	__syncthreads();
	t[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx]-t[tx][BL];
	t[tx][ty]*=t[tx][ty];
	__syncthreads();

	size_t stride=N2/2;

	if(tx<stride) t[tx][ty]+=t[tx+stride][ty];
	__syncthreads();
	
	stride=(stride+1)/2;
	if(tx>0 && tx<stride) t[tx][ty]+=t[tx+stride-1][ty];
	__syncthreads();

	for(stride/=2;stride>0; stride/=2){
		if(tx<stride) t[tx][ty]+=t[tx+stride][ty];
		__syncthreads();
	}

	if(tx==0) dif[by*L+bx*BL+ty]=sqrt(t[0][ty]); 
}


__global__ void
cudaExCoalesce(float *a, float *dif, int L, int nb){

	__shared__ float t[N2][BL+1];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	int tx=threadIdx.x;
	int ty=threadIdx.y;

	t[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx];
	if(ty==0) t[tx][BL]=a[by*N2*L+tx];

	__syncthreads();
	
	float del=t[tx][ty]-t[tx][BL];
	t[tx][ty]=del*del;

	__syncthreads();

	size_t stride=N2/2;

	if(tx<stride) t[tx][ty]+=t[tx+stride][ty];
	__syncthreads();
	
	stride=(stride+1)/2;
	if(tx>0 && tx<stride) t[tx][ty]+=t[tx+stride-1][ty];
	__syncthreads();

	for(stride/=2;stride>0; stride/=2){
		if(tx<stride) t[tx][ty]+=t[tx+stride][ty];
		__syncthreads();
	}

	if(tx==0) dif[by*L+bx*BL+ty]=sqrt(t[0][ty]); 
}


__global__ void
cudaEx(float *a, float *dif, int L, int nb){

	__shared__ float t[N2][BL];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	int tx=threadIdx.x;
	int ty=threadIdx.y;

	t[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx]-a[by*N2*L+tx];
	t[tx][ty]*=t[tx][ty];

	__syncthreads();
	
	size_t stride=N2/2;

	if(tx<stride) t[tx][ty]+=t[tx+stride][ty];
	__syncthreads();
	
	stride=(stride+1)/2;
	if(tx>0 && tx<stride) t[tx][ty]+=t[tx+stride-1][ty];
	__syncthreads();

	for(stride/=2;stride>0; stride/=2){
		if(tx<stride) t[tx][ty]+=t[tx+stride][ty];
		__syncthreads();
	}

	if(tx==0) dif[by*L+bx*BL+ty]=sqrt(t[0][ty]); 
}


*/

#if 0
void 
gpuExplore(float *a, float *dif, int L, int nb){
	/*definition of time variable*/
	struct stopwatch_t* timer = NULL;
	long double t_gpu;
        /* Setup timers */
        stopwatch_init ();
        timer = stopwatch_create ();

	float *da, *ddif;
	size_t size1=N2*L*nb*sizeof(float);
	size_t size2=L*nb*sizeof(float);
	
	cudaMalloc((void**)&da, size1);
	cudaMalloc((void**)&ddif, size2);
	cudaMemcpy(da,a,size1, cudaMemcpyHostToDevice);

	dim3 ngrid(L/BL, nb);
	dim3 nblock(N2,BL);

	stopwatch_start (timer);
	//cudaExCoalesce<<<ngrid, nblock>>>(da,ddif,L,nb);
	//cudaExCoalesceSub<<<ngrid, nblock>>>(da,ddif,L,nb);
	cudaExCoalesceSubUnsyn<<<ngrid, nblock>>>(da,ddif,L,nb);
	//cudaEx<<<ngrid, nblock>>>(da,ddif,L,nb);
	cudaThreadSynchronize();
	t_gpu = stopwatch_stop (timer);

	cudaMemcpy(dif, ddif, size2,cudaMemcpyDeviceToHost);

	fprintf (stderr, "GPU distance: %Lg secs ==> %Lg billion elements/second\n", t_gpu, (N2*L*nb) / t_gpu * 1e-9 );

	cudaFree(da);
	cudaFree(ddif);
}

#endif

int
cmp(float *a1, float *a2, float *b1, float *b2, int *c, int *d, int N){
	int s1=0;
	int s2=0;
	int s3=0;
	for(int i=0; i<N; i++){
		if(abs(a1[i]-a2[i]>1e-6)){ 
			s1++;
			fprintf(stderr,"%d\t%f\t%f\n", i,a1[i],a2[i]);
		}
	}
	for(int i=0; i<N; i++){
		if(abs(b1[i]-b2[i]>1e-6)){ 
			s2++;
			fprintf(stderr,"%d\t%f\t%f\n", i,b1[i],b2[i]);
		}
	}
	for(int i=0; i<N; i++){
		if(c[i]!=d[i]){
			s3++;
			fprintf(stderr, "%d\t%d\t%d\n", i,c[i],d[i]);
		}
	}
	
	return s1+s2+s3;
}

