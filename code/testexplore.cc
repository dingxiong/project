#include "preexplore.h"
#include "timer.h"
#include <iostream>
#include <cstdlib>
using std::cout;
using std::endl;
int main(){
	/*l must be multipliers of NB*/
	const int L=10240;
	//const int L=64;
	const int nb=1024;
	//const int nb=4;
	float *a,*b1,*b2,*c1,*c2;
	int *la1,*la2;
	//float a[N2*L*nb],b[L*nb],c[L*nb];
	a=(float*)malloc(N2*L*nb*sizeof(float));
	b1=(float*)malloc(L*nb*sizeof(float));
	b2=(float*)malloc(L*nb*sizeof(float));
	c1=(float*)malloc(L*nb*sizeof(float));
	c2=(float*)malloc(L*nb*sizeof(float));
	la1=(int*)malloc(L*nb*sizeof(int));
	la2=(int*)malloc(L*nb*sizeof(int));
	srand(time(NULL));
	for(size_t i=0; i<N2*L*nb; i++){
		a[i]=(float)rand()/RAND_MAX*2-1.0;	
	}

	int err;
	cpuExplore(a,b1,b2,la1,L,nb,1);
	gpuExplore(a,c1,c2,la2,L,nb,6);
	//cpuExplore(a,c1,c2,la2,L,nb,2);
	err=cmp(b1,c1,b2,c2,la1,la2,L*nb);
	cout<<err<<endl;

	//cpuExplore(a,b1,b2,la1,L,nb,3);
	//cpuExplore(a,c1,c2,la2,L,nb,4);
	//err=cmp(b1,c1,b2,c2,la1,la2,L*nb);
	//cout<<err<<endl;
//	gpuExplore(a,c,L,nb);
	for(size_t i=0; i<16*2; i++ ) cout<<b2[i]<<endl;
	cout<<endl;
	for(size_t i=0; i<16*2; i++ ) cout<<c2[i]<<endl;
	std::cout.precision(8);
	//for(size_t i=0; i<L*nb; i++ ) cout<<b[i]<<endl;
	//for(size_t i=0; i<L*nb; i++ ) cout<<c[i]<<endl;
	
	return 0;
}
