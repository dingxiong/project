#include "preexplore.h"

__global__ void
cudaSingleX2(float *a, float *dif1, float *dif2, int *label, int L, int nb){
	__shared__ float start[N2];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	//int tx=threadIdx.x;
	int ty=threadIdx.y;

	if(ty==0){
		for(size_t i=0; i<N2; i++) start[i]=a[by*N2*L+i];
	}
	__syncthreads();
	
	float sub[N2][2];
	float s1[2]={0,0};
	float s2[2]={0,0};
	for(size_t i=0; i<N2; ++i) {
		sub[i][0]=a[by*N2*L+2*bx*BL*N2+2*ty*N2+i];
		sub[i][1]=a[by*N2*L+2*bx*BL*N2+(2*ty+1)*N2+i];
	}
	for(size_t i=0; i<N2; ++i){
		s1[0]+= (sub[i][0]-start[i])*(sub[i][0]-start[i]);
		s2[0]+= (sub[i][0]+start[i])*(sub[i][0]+start[i]);
		s1[1]+= (sub[i][1]-start[i])*(sub[i][1]-start[i]);
		s2[1]+= (sub[i][1]+start[i])*(sub[i][1]+start[i]);
	}
	dif1[by*L+2*bx*BL+2*ty]=sqrt(s1[0]);
	dif1[by*L+2*bx*BL+2*ty+1]=sqrt(s1[1]);
	dif2[by*L+2*bx*BL+2*ty]=sqrt(s2[0]);
	dif2[by*L+2*bx*BL+2*ty+1]=sqrt(s2[1]);

	if(dif1[by*L+2*bx*BL+2*ty]< tol && dif2[by*L+2*bx*BL+2*ty]< tol) label[by*L+2*bx*BL+2*ty]=0;
	else if(dif1[by*L+2*bx*BL+2*ty]< tol && dif2[by*L+2*bx*BL+2*ty]> tol) label[by*L+2*bx*BL+2*ty]=1;
	else if(dif1[by*L+2*bx*BL+2*ty]> tol && dif2[by*L+2*bx*BL+2*ty]< tol) label[by*L+2*bx*BL+2*ty]=2;
	else  label[by*L+2*bx*BL+2*ty]=3;
	
	if(dif1[by*L+2*bx*BL+2*ty+1]< tol && dif2[by*L+2*bx*BL+2*ty+1]< tol) label[by*L+2*bx*BL+2*ty+1]=0;
	else if(dif1[by*L+2*bx*BL+2*ty+1]< tol && dif2[by*L+2*bx*BL+2*ty+1]> tol) label[by*L+2*bx*BL+2*ty+1]=1;
	else if(dif1[by*L+2*bx*BL+2*ty+1]> tol && dif2[by*L+2*bx*BL+2*ty+1]< tol) label[by*L+2*bx*BL+2*ty+1]=2;
	else  label[by*L+2*bx*BL+2*ty+1]=3;
	
}

__global__ void
cudaSingle(float *a, float *dif1, float *dif2, int *label, int L, int nb){
	__shared__ float start[N2];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	//int tx=threadIdx.x;
	int ty=threadIdx.y;

	if(ty==0){
		for(size_t i=0; i<N2; i++) start[i]=a[by*N2*L+i];
	}
	__syncthreads();
	
	float sub[N2];
	float s1=0;
	float s2=0;
	for(size_t i=0; i<N2; ++i) sub[i]=a[by*N2*L+bx*BL*N2+ty*N2+i];
	for(size_t i=0; i<N2; ++i){
		s1+= (sub[i]-start[i])*(sub[i]-start[i]);
		s2+= (sub[i]+start[i])*(sub[i]+start[i]);
	}
	dif1[by*L+bx*BL+ty]=sqrt(s1);
	dif2[by*L+bx*BL+ty]=sqrt(s2);

	if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=0;
	else if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]> tol) label[by*L+bx*BL+ty]=1;
	else if(dif1[by*L+bx*BL+ty]> tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=2;
	else  label[by*L+bx*BL+ty]=3;
	
}
__global__ void
cudaExCoalesceSubUnsyn(float *a, float *dif1, float *dif2, int *label, int L, int nb){

	__shared__ float t[N2][BL+1];
	__shared__ float p[N2][BL];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	int tx=threadIdx.x;
	int ty=threadIdx.y;

	if(ty==0) t[tx][BL]=a[by*N2*L+tx];
	__syncthreads();
	t[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx];
	p[tx][ty]=t[tx][ty]+t[tx][BL];
	p[tx][ty]*=p[tx][ty];
	t[tx][ty] -= t[tx][BL];
	t[tx][ty]*=t[tx][ty];
	__syncthreads();


	size_t stride=N2/2;

	if(tx<stride) {
		t[tx][ty]+=t[tx+stride][ty];
		p[tx][ty]+=p[tx+stride][ty];
	}
	__syncthreads();
	
	stride=(stride+1)/2;
	if(tx>0 && tx<stride) {
		t[tx][ty]+=t[tx+stride-1][ty];
		p[tx][ty]+=p[tx+stride-1][ty];
	}
	__syncthreads();
	
	for(stride/=2;stride>8; stride/=2){
		if(tx<stride) {
			t[tx][ty]+=t[tx+stride][ty];
			p[tx][ty]+=p[tx+stride][ty];
		}
		__syncthreads();
	}
	

	if(tx==0){
		 dif1[by*L+bx*BL+ty]=sqrt(t[0][ty]+t[1][ty]+t[2][ty]+t[3][ty]
					+t[4][ty]+t[5][ty]+t[6][ty]+t[7][ty]); 
		 dif2[by*L+bx*BL+ty]=sqrt(p[0][ty]+p[1][ty]+p[2][ty]+p[3][ty]
					+p[4][ty]+p[5][ty]+p[6][ty]+p[7][ty]); 
		if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=0;
		else if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]> tol) label[by*L+bx*BL+ty]=1;
		else if(dif1[by*L+bx*BL+ty]> tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=2;
		else  label[by*L+bx*BL+ty]=3;
		
	}
}



__global__ void
cudaExCoalesceSub(float *a, float *dif1, float *dif2, int *label, int L, int nb){

	__shared__ float t[N2][BL+1];
	__shared__ float p[N2][BL];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	int tx=threadIdx.x;
	int ty=threadIdx.y;

	if(ty==0) t[tx][BL]=a[by*N2*L+tx];
	__syncthreads();
	t[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx];
	p[tx][ty]=t[tx][ty]+t[tx][BL];
	p[tx][ty]*=p[tx][ty];
	t[tx][ty] -= t[tx][BL];
	t[tx][ty]*=t[tx][ty];
	__syncthreads();

	size_t stride=N2/2;

	if(tx<stride) {
		t[tx][ty]+=t[tx+stride][ty];
		p[tx][ty]+=p[tx+stride][ty];
	}
	__syncthreads();
	
	stride=(stride+1)/2;
	if(tx>0 && tx<stride) {
		t[tx][ty]+=t[tx+stride-1][ty];
		p[tx][ty]+=p[tx+stride-1][ty];
	}
	__syncthreads();

	for(stride/=2;stride>0; stride/=2){
		if(tx<stride) {
			t[tx][ty]+=t[tx+stride][ty];
			p[tx][ty]+=p[tx+stride][ty];
		}
		__syncthreads();
	}

	if(tx==0) {
		dif1[by*L+bx*BL+ty]=sqrt(t[0][ty]); 
		dif2[by*L+bx*BL+ty]=sqrt(p[0][ty]); 

		if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=0;
		else if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]> tol) label[by*L+bx*BL+ty]=1;
		else if(dif1[by*L+bx*BL+ty]> tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=2;
		else  label[by*L+bx*BL+ty]=3;
	}

}


__global__ void
cudaExCoalesce(float *a, float *dif1, float *dif2, int *label, int L, int nb){

	__shared__ float t[N2][BL+1];
	__shared__ float p[N2][BL];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	int tx=threadIdx.x;
	int ty=threadIdx.y;

	t[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx];
	if(ty==0) t[tx][BL]=a[by*N2*L+tx];

	__syncthreads();
	
	float del=t[tx][ty]-t[tx][BL];
	float pls=t[tx][ty]+t[tx][BL];
	t[tx][ty]=del*del;
	p[tx][ty]=pls*pls;

	__syncthreads();

	size_t stride=N2/2;

	if(tx<stride) {
		t[tx][ty]+=t[tx+stride][ty];
		p[tx][ty]+=p[tx+stride][ty];
	}
		
	__syncthreads();
	
	stride=(stride+1)/2;
	if(tx>0 && tx<stride) {
		t[tx][ty]+=t[tx+stride-1][ty];
		p[tx][ty]+=p[tx+stride-1][ty];
	}
	__syncthreads();

	for(stride/=2;stride>0; stride/=2){
		if(tx<stride) {
			t[tx][ty]+=t[tx+stride][ty];
			p[tx][ty]+=p[tx+stride][ty];
		}
		__syncthreads();
	}

	if(tx==0) {
		dif1[by*L+bx*BL+ty]=sqrt(t[0][ty]); 
		dif2[by*L+bx*BL+ty]=sqrt(p[0][ty]); 

		if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=0;
		else if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]> tol) label[by*L+bx*BL+ty]=1;
		else if(dif1[by*L+bx*BL+ty]> tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=2;
		else  label[by*L+bx*BL+ty]=3;
	}
}


__global__ void
cudaEx(float *a, float *dif1, float *dif2, int *label, int L, int nb){

	__shared__ float t[N2][BL];
	__shared__ float p[N2][BL];

	int bx=blockIdx.x;
	int by=blockIdx.y;
	int tx=threadIdx.x;
	int ty=threadIdx.y;

	t[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx]-a[by*N2*L+tx];
	t[tx][ty]*=t[tx][ty];
	p[tx][ty]=a[by*N2*L+bx*BL*N2+ty*N2+tx]+a[by*N2*L+tx];
	p[tx][ty]*=p[tx][ty];

	__syncthreads();
	
	size_t stride=N2/2;

	if(tx<stride) {
		t[tx][ty]+=t[tx+stride][ty];
		p[tx][ty]+=p[tx+stride][ty];
	}
	__syncthreads();
	
	stride=(stride+1)/2;
	if(tx>0 && tx<stride) {
		t[tx][ty]+=t[tx+stride-1][ty];
		p[tx][ty]+=p[tx+stride-1][ty];
	}
	__syncthreads();

	for(stride/=2;stride>0; stride/=2){
		if(tx<stride){
			 t[tx][ty]+=t[tx+stride][ty];
			 p[tx][ty]+=p[tx+stride][ty];
		}
		__syncthreads();
	}

	if(tx==0) {
		dif1[by*L+bx*BL+ty]=sqrt(t[0][ty]); 
		dif2[by*L+bx*BL+ty]=sqrt(p[0][ty]); 

		if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=0;
		else if(dif1[by*L+bx*BL+ty]< tol && dif2[by*L+bx*BL+ty]> tol) label[by*L+bx*BL+ty]=1;
		else if(dif1[by*L+bx*BL+ty]> tol && dif2[by*L+bx*BL+ty]< tol) label[by*L+bx*BL+ty]=2;
		else  label[by*L+bx*BL+ty]=3;
	}
}



void 
gpuExplore(float *a, float *dif1, float *dif2, int *label, int L, int nb, int mode){
	/*definition of time variable*/
	struct stopwatch_t* timer = NULL;
	long double t_gpu;
        /* Setup timers */
        stopwatch_init ();
        timer = stopwatch_create ();

	float *da, *ddif1, *ddif2;
	int *dlabel;
	size_t size1=N2*L*nb*sizeof(float);
	size_t size2=L*nb*sizeof(float);
	
	cudaMalloc((void**)&da, size1);
	cudaMalloc((void**)&ddif1, size2);
	cudaMalloc((void**)&ddif2, size2);
	cudaMalloc((void**)&dlabel, size2);
	cudaMemcpy(da,a,size1, cudaMemcpyHostToDevice);

	dim3 ngrid(L/BL, nb);
	dim3 nblock(N2,BL);

	dim3 ngrid2(L/(2*BL), nb);
	dim3 nblock2(1,BL);

	stopwatch_start (timer);
	switch(mode){
		case 1 : cudaEx<<<ngrid, nblock>>>(da,ddif1,ddif2,dlabel,L,nb); break;
		case 2 : cudaExCoalesce<<<ngrid, nblock>>>(da,ddif1,ddif2,dlabel,L,nb); break;
		case 3 : cudaExCoalesceSub<<<ngrid, nblock>>>(da,ddif1,ddif2,dlabel,L,nb); break;
		case 4 : cudaExCoalesceSubUnsyn<<<ngrid, nblock>>>(da,ddif1,ddif2,dlabel,L,nb); break;
		case 5 : cudaSingle<<<ngrid, nblock2>>>(da,ddif1,ddif2,dlabel,L,nb); break;
		case 6 : cudaSingleX2<<<ngrid2, nblock2>>>(da,ddif1,ddif2,dlabel,L,nb); break;
		default:
			cudaEx<<<ngrid, nblock>>>(da,ddif1,ddif2,dlabel,L,nb); break;
	}
	cudaThreadSynchronize();
	t_gpu = stopwatch_stop (timer);

	cudaMemcpy(dif1, ddif1, size2,cudaMemcpyDeviceToHost);
	cudaMemcpy(dif2, ddif2, size2,cudaMemcpyDeviceToHost);
	cudaMemcpy(label, dlabel, size2,cudaMemcpyDeviceToHost);

	fprintf (stderr, "GPU distance %d : %Lg secs ==> %Lg billion elements/second\n", mode, t_gpu, (N2*L*nb) / t_gpu * 1e-9 );

	cudaFree(da);
	cudaFree(ddif1);
	cudaFree(ddif2);
	cudaFree(dlabel);
}

